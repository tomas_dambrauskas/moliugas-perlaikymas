package lt.akademija.perlaikymas.jpa.entities.repositories.jpa;

import lt.akademija.perlaikymas.jpa.entities.Coupon;
import lt.akademija.perlaikymas.jpa.entities.repositories.CouponRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CouponDaoImpl implements CouponRepository {
	static final Logger log = LoggerFactory.getLogger(CouponDaoImpl.class);

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	@Override
	public List<Coupon> findAll() {

		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
			Root<Coupon> root = cq.from(Coupon.class);
			cq.select(root);
			TypedQuery<Coupon> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void save(Coupon newCoupon) {

		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(newCoupon))
				newCoupon = entityManager.merge(newCoupon);
			entityManager.persist(newCoupon);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Coupon coupon) {

		EntityManager entityManager = getEntityManager();
		
		try {
			entityManager.getTransaction().begin();
			coupon = entityManager.merge(coupon);
			entityManager.remove(coupon);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}


}