package lt.akademija.perlaikymas.jpa.entities.repositories;

import lt.akademija.perlaikymas.jpa.entities.Item;

public interface ItemRepository {

	public void insertOrUpdate(Item item);
	public void delete(Item item);
	public void deleteById(Long itemId);
	public Long countAllItems();
}
