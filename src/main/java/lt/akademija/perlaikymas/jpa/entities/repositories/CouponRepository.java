package lt.akademija.perlaikymas.jpa.entities.repositories;

import lt.akademija.perlaikymas.jpa.entities.Coupon;

import java.util.List;

public interface CouponRepository {

	public List<Coupon> findAll();

	public void save(Coupon newCoupon);

	public void delete(Coupon coupon);

}
