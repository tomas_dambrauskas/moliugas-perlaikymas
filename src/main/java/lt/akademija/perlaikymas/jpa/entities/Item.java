package lt.akademija.perlaikymas.jpa.entities;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Table(indexes = @Index(columnList = "name ASC", name = "ITEM_NAME_IDX", unique=false), name = "ITEM")
@Entity
public class Item implements Serializable {
	private static final long serialVersionUID = -2592264505437536988L;
	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@NotBlank
	private String name;

//	@NotNull
//	@Column(name = "CLASS", nullable = false)
//	private Units units = Units.KG;
//
	@NotNull
	@NotBlank
	private String units;

	@NotNull
	private double quantity;

	@NotNull
	private double price;

	@JoinColumn(name = "coupon_id")
	@ManyToOne(optional = true, cascade = { CascadeType.ALL })
	private Coupon coupon;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
		coupon.addItem(this);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Item item = (Item) o;

		if (Double.compare(item.quantity, quantity) != 0) return false;
		if (Double.compare(item.price, price) != 0) return false;
		if (id != null ? !id.equals(item.id) : item.id != null) return false;
		if (name != null ? !name.equals(item.name) : item.name != null) return false;
		if (units != item.units) return false;
		return coupon != null ? coupon.equals(item.coupon) : item.coupon == null;

	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (units != null ? units.hashCode() : 0);
		temp = Double.doubleToLongBits(quantity);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(price);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (coupon != null ? coupon.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Item{" +
				"id=" + id +
				", name='" + name + '\'' +
				", units=" + units +
				", quantity=" + quantity +
				", price=" + price +
				", coupon=" + coupon +
				'}';
	}
}
