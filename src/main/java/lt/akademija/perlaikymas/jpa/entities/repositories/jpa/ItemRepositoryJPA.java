package lt.akademija.perlaikymas.jpa.entities.repositories.jpa;

import lt.akademija.perlaikymas.jpa.entities.Item;
import lt.akademija.perlaikymas.jpa.entities.repositories.ItemRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class ItemRepositoryJPA implements ItemRepository {

	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}


	@Override
	public void insertOrUpdate(Item item) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(item))
				item = entityManager.merge(item);
			entityManager.persist(item);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Item item) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(item);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void deleteById(Long itemId) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			Item item = entityManager.find(Item.class, itemId);
			if (item != null)
				entityManager.remove(item);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Long countAllItems() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
			countQuery.select(cb.count(countQuery.from(Item.class)));
			TypedQuery<Long> q = entityManager.createQuery(countQuery);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

}
