package lt.akademija.perlaikymas.jpa.entities;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(indexes = @Index(columnList = "buyer ASC, seller ASC", name = "COUP_NAME_IDX", unique=false), name = "COUPON")
public class Coupon implements Serializable {

	private static final long serialVersionUID = 5089035398648855772L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@NotBlank
	private String buyer;

	@NotNull
	@NotBlank
	private String seller;

	@NotNull
	@Temporal(TemporalType.DATE)
	private Date date;

	@OneToMany(mappedBy = "coupon", fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	private List<Item> items;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Item> getItems() {
		return items;
	}

	public void addItem(Item b) {
		if (getItems() == null)
			setItems(new ArrayList<>());
		if (!getItems().contains(b))
			getItems().add(b);
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Coupon coupon = (Coupon) o;

		if (id != null ? !id.equals(coupon.id) : coupon.id != null) return false;
		if (buyer != null ? !buyer.equals(coupon.buyer) : coupon.buyer != null) return false;
		if (seller != null ? !seller.equals(coupon.seller) : coupon.seller != null) return false;
		if (date != null ? !date.equals(coupon.date) : coupon.date != null) return false;
		return items != null ? items.equals(coupon.items) : coupon.items == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (buyer != null ? buyer.hashCode() : 0);
		result = 31 * result + (seller != null ? seller.hashCode() : 0);
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (items != null ? items.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Coupon{" +
				"id=" + id +
				", buyer='" + buyer + '\'' +
				", seller='" + seller + '\'' +
				", date=" + date +
				", items=" + items +
				'}';
	}
}
