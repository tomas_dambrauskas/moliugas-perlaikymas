package lt.akademija.perlaikymas.jpa.entities.enums;

public enum Units {
    KG("kilograms"),
    M("metres");

    private String label;

    private Units(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }


}
