package lt.akademija.perlaikymas.ui.controllers;

import lt.akademija.perlaikymas.jpa.entities.Item;
import lt.akademija.perlaikymas.jpa.entities.Coupon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.akademija.perlaikymas.jpa.entities.repositories.CouponRepository;
import lt.akademija.perlaikymas.jpa.entities.repositories.ItemRepository;

public class AddNewItemPageBean {
	static final Logger log = LoggerFactory.getLogger(AddNewItemPageBean.class);
	/**
	 * Repositories
	 */
	private CouponRepository couponRepo;
	private ItemRepository itemRepo;

	/**
	 * Other pages for functionality re-use
	 */
	private CouponListPageBean couponListPageBean;
	
	/**
	 * Holds data for entering new item information like title and release date.
	 * In other words - it backs up form for new item.
	 */
	private Item newItem;
	
	/**
	 * Initialization method for bean
	 */
	public void init() {
		newItem = new Item();
	}
	
	public String addNew() {
		/**
		 * At this point, item data is stored in newItem variable, we can save it to DB
		 */
		Coupon coupon = couponListPageBean.getData().getCurrentCoupon();
		log.debug("Before saving item, got coupon: {}", coupon);
		log.debug("New item data: {}", newItem);
		newItem.setCoupon(coupon);
		coupon.addItem(newItem);
		couponRepo.save(coupon);
		
		return CouponListPageBean.NAV_LIST_COUPONS;
	}
	
	public CouponListPageBean getCouponListPageBean() {
		return couponListPageBean;
	}
	public void setCouponListPageBean(CouponListPageBean couponListPageBean) {
		this.couponListPageBean = couponListPageBean;
	}
	public CouponRepository getCouponRepo() {
		return couponRepo;
	}
	public void setCouponRepo(CouponRepository couponRepo) {
		this.couponRepo = couponRepo;
	}
	public ItemRepository getItemRepo() {
		return itemRepo;
	}
	public void setItemRepo(ItemRepository getItemRepo) {
		this.itemRepo = getItemRepo;
	}
	public Item getNewItem() {
		return newItem;
	}
	public void setNewItem(Item newItem) {
		this.newItem = newItem;
	}
}
