package lt.akademija.perlaikymas.ui.controllers;

import lt.akademija.perlaikymas.jpa.entities.Coupon;
import lt.akademija.perlaikymas.jpa.entities.repositories.CouponRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CouponListPageBean {

	static final Logger log = LoggerFactory.getLogger(CouponListPageBean.class);

	/**
	 * Navigation outcomes
	 */
	public static final String NAV_SHOW_ADD_ITEM = "show-add-item";
	public static final String NAV_LIST_COUPONS = "coupon-list";
	public static final String NAV_SHOW_EDIT_COUPON = "show-edit-coupon";
	public static final String NAV_SHOW_VIEW_COUPON = "show-view-coupon";

	/**
	 * This class holds user-input data. Basically it is just container. This
	 * class is responsible JUST FOR HOLDING ANY DATA in session
	 */
	public static class CouponListPageData implements Serializable {

		private static final long serialVersionUID = -7847613490719023414L;

		/**
		 * Holds data of new Coupon form fields
		 */
		@Valid
		private Coupon newCoupon;

		/**
		 * Holds data for currently selected coupon
		 */
		@Valid
		private Coupon currentCoupon;

		private List<Coupon> foundCoupons;

		/**
		 * Initialization method for bean (called after bean is created)
		 */
		public void init() {
			newCoupon = new Coupon();
			foundCoupons = new ArrayList<>();
		}

		public Coupon getNewCoupon() {
			return newCoupon;
		}

		public void setNewCoupon(Coupon newCoupon) {
			this.newCoupon = newCoupon;
		}

		public Coupon getCurrentCoupon() {
			return currentCoupon;
		}

		public void setCurrentCoupon(Coupon currentCoupon) {
			this.currentCoupon = currentCoupon;
		}

		public List<Coupon> getFoundCoupons() {
			return foundCoupons;
		}

		public void setFoundCoupons(List<Coupon> foundCoupons) {
			this.foundCoupons = foundCoupons;
		}

	}// end of CouponListPageData

	/**
	 * Data container/holder
	 */
	private CouponListPageData data;

	/**
	 * repositories
	 */
	private CouponRepository couponRepo;

	/**
	 * Add new coupon to DB as it was entered in form
	 *
	 * @return JSF outcome
	 */
	public String addNew() {
		couponRepo.save(data.newCoupon);
		data.newCoupon = new Coupon(); // clear the form values
		return NAV_LIST_COUPONS;
	}

	public String editCoupon() {
		return NAV_SHOW_EDIT_COUPON;
	}

	public String viewCoupon() {
		return NAV_SHOW_VIEW_COUPON;
	}

	/**
	 * Delete selected coupon
	 *
	 * @param coupon
	 * @return JSF outcome
	 */
	public String deleteSelected(Coupon coupon) {
		if (coupon == null)
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Coupon being deleted is null?"));
		else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "You are deleting coupon!"));
			couponRepo.delete(coupon);
		}
		return NAV_LIST_COUPONS;
	}

	/**
	 * This method just remembers selected coupon and redirects user to new page
	 */
	public String showAddItemPage(Coupon a) {
		log.debug("Will store selected coupon for later access in Add new Item form: {}", a);
		data.currentCoupon = a;
		return NAV_SHOW_ADD_ITEM;
	}

	public CouponListPageData getData() {
		return data;
	}

	public void setData(CouponListPageData data) {
		this.data = data;
	}

	public CouponRepository getCouponRepo() {
		return couponRepo;
	}

	public void setCouponRepo(CouponRepository couponRepo) {
		this.couponRepo = couponRepo;

	}
	public List<Coupon> getCouponList() {
		return couponRepo.findAll();
	}

}

